package eu.gaiax.wizard.api.model;


public record RequiredActionsTokenDto (String token) {
}
