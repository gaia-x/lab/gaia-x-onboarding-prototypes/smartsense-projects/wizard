package eu.gaiax.wizard.api.model;

public record VerifiableCredentialProofDto(SignatureDto proof) {
}
